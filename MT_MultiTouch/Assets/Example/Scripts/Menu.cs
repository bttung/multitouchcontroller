﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	private void OnGUI()
	{
		if (Application.loadedLevelName == "Menu")
		{
//			float height = 100;

			if (GUI.Button(new Rect(Screen.width / 2 - 75, 100, 150, 100), "Tap"))
			{
				Application.LoadLevel("Tap");
			}
			if (GUI.Button(new Rect(Screen.width / 2 - 75, 250, 150, 100), "Pan"))
			{
				Application.LoadLevel("Pan");
			}
			if (GUI.Button(new Rect(Screen.width / 2 - 75, 400, 150, 100), "LongPress"))
			{
				Application.LoadLevel("LongPress");
			}
			if (GUI.Button(new Rect(Screen.width / 2 - 75, 550, 150, 100), "Flick"))
			{
				Application.LoadLevel("Flick");
			}
			if (GUI.Button(new Rect(Screen.width / 2 - 75, 700, 150, 100), "Pan_Scale_Rotate"))
			{
				Application.LoadLevel("Pan_Scale_Rotate");
			}
			if (GUI.Button(new Rect(Screen.width / 2 - 75, 850, 150, 100), "DoubleTap"))
			{
				Application.LoadLevel("DoubleTap");
			}	
		}

		if (Application.loadedLevelName != "Menu")
		{
			if (GUI.Button(new Rect(Screen.width - 200, 100, 150, 100), "Menu"))
			{
				Application.LoadLevel("Menu");
			}
		}
	}

}


