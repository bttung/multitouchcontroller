﻿using UnityEngine;
using System.Collections;
using MTMultiTouch.Gestures;

[RequireComponent(typeof(TapGesture))]
public class TapController : MonoBehaviour 
{

	private void OnEnable ()
	{
		GetComponent<TapGesture>().StateChanged += onTap;
	}

	private void OnDisable ()
	{
		GetComponent<TapGesture>().StateChanged -= onTap;
	}

	private void onTap (object sender, GestureStateChangeEventArgs e)
	{
		if (e.State == Gesture.GestureState.Recognized)
		{
			Debug.Log("Tap Recoginzed");
			Color color = new Color(Random.value, Random.value, Random.value);
			gameObject.transform.renderer.material.color = color;
		}
	}
	
}
