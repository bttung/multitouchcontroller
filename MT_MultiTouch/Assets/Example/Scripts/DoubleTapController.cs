﻿using System;
using UnityEngine;
using System.Collections;
using MTMultiTouch.Gestures;
using Random = UnityEngine.Random;

[RequireComponent(typeof(TapGesture))]
public class DoubleTapController : MonoBehaviour {

	private void OnEnable()
	{
		foreach (var tap in GetComponents<TapGesture>())
		{
			tap.Tapped += onTapped;
		}
	}

	private void OnDisable()
	{
		foreach (var tap in GetComponents<TapGesture>())
		{
			tap.Tapped -= onTapped;
		}
	}

	private void onTapped(object sender, EventArgs e)
	{
		var tap = sender as TapGesture;
		switch (tap.NumberOfTapsRequired)
		{
		case 1:
			Debug.Log("one tap");
			break;
		case 2:
			Debug.Log("two tap");
			Color color = new Color(Random.value, Random.value, Random.value);
			gameObject.transform.renderer.material.color = color;
			break;
		default:
			break;
		}
	}

}
