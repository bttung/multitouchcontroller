﻿using System;
using UnityEngine;
using System.Collections;
using MTMultiTouch.Gestures;
using Random = UnityEngine.Random;

[RequireComponent(typeof(PressGesture))]
[RequireComponent(typeof(ReleaseGesture))]
[RequireComponent(typeof(LongPressGesture))]
public class LongPressController : MonoBehaviour 
{
	public GameObject plane;
	private Vector3 initialPos;
	private Color initialColor;

	private void OnEnable ()
	{
		initialPos = transform.position;
//		initialColor = plane.gameObject.transform.renderer.material.color;

		GetComponent<PressGesture>().Pressed += onPressed;
		GetComponent<ReleaseGesture>().Released += onReleased;
		GetComponent<LongPressGesture>().StateChanged += onLongPressStateChanged;
	}

	private void OnDisable()
	{
		GetComponent<PressGesture>().Pressed -= onPressed;
		GetComponent<ReleaseGesture>().Released -= onReleased;
		GetComponent<LongPressGesture>().StateChanged -= onLongPressStateChanged;
	}

	private void onPressed(object sender, EventArgs e)
	{
		Debug.Log("button Pressed");
		gameObject.transform.localPosition = new Vector3(0, -gameObject.transform.localScale.y * 0.001f, 0);
	}

	private void onReleased(object sender, EventArgs e)
	{
		Debug.Log("Released");
		gameObject.transform.position = initialPos;
		plane.gameObject.transform.renderer.material.color = new Color(0, 0, 0);
	}

	private void onLongPressStateChanged(object sender, GestureStateChangeEventArgs e)
	{
		Debug.Log("LongPressed");
		Color color = new Color(Random.value, Random.value, Random.value);
		plane.gameObject.transform.renderer.material.color = color;
	}

}
