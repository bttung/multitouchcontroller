﻿using System;
using UnityEngine;
using System.Collections;
using MTMultiTouch.Gestures;

[RequireComponent(typeof(FlickGesture))]
[RequireComponent(typeof(PressGesture))]
public class FlickController : MonoBehaviour {

	private Quaternion bodyRotation;
	private Vector3 speed;

	private void OnEnable()
	{
		GetComponent<FlickGesture>().StateChanged += onFlicked;
		GetComponent<PressGesture>().Pressed += onPressed;
	}

	private void OnDisable()
	{
		GetComponent<FlickGesture>().StateChanged -= onFlicked;
		GetComponent<PressGesture>().Pressed -= onPressed;
	}

	private void FixedUpdate()
	{
		if (speed != Vector3.zero)
		{
			rigidbody.AddTorque(speed);
			speed = Vector2.zero;
		}
	}

	private void onFlicked(object sender, GestureStateChangeEventArgs e)
	{
		if (e.State == Gesture.GestureState.Recognized)
		{
			Debug.Log ("Flicked");
			var flickGesture = sender as FlickGesture;
			var spd = flickGesture.ScreenFlickVector / flickGesture.ScreenFlickTime;
			speed = new Vector3(spd.y, - spd.x, 0);
		}
	}
	
	private void onPressed(object sender, EventArgs e)
	{
		Debug.Log("Pressed");
		rigidbody.angularVelocity = Vector3.zero;
	}

}
