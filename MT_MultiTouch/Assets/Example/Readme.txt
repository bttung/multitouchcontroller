○サンプルタッチ一覧
　Tap
  Pan
  LongPress
  Flick
  Pan&Scale&Rotate
  DoubleTap

○Tapシーン
　キュブにタップして、指を画面に離すとキュブの色が変わる

○Panシーン
　指を移動すると、キュブが指の動きに沿い、移動する

○LongPressシーン
　キュブの上に押すと、キュブが下に下がり、平面の色が変わる。
　キュブを1.5秒以上押し続けると、平面の色が変わり、指を離すと平面の色は黒くなる。

○Flickシーン
　指をキュブ上にスワイプすると、キュブが回転する。
　キュブが回転する最中、キュブを押すと、キュブは止める。

○Pan&Scale&Rotate
  同時にマルチジェスチャーを認識する。
  指一本を移動させると、キュブが移動する。
  指二本でキュブを拡大・縮小または回転できる。
  Pan・Scale・Rotateジェスチャーは切り替えられる。

○DoubleTapシーン
　２回クリックすると、キュブの色が変わる。

○実装方法
　GetComponent<TapGesture>().StateChanged += ハンドラー
　GetComponent<FlickGesture>().StateChanged += ハンドラー
　GetComponent<PressGesture>().Pressed += ハンドラー
　GetComponent<ReleaseGesture>().Released += ハンドラー
　GetComponent<LongPressGesture>().StateChanged += ハンドラー
  ハンドラーのなかに、行いたい処理を行う。

 ○その他
 　Gesture.ScreenPosition: タッチした位置を格納するベクター
 　FlickGesture.ScreenFlickTime: スワイプした時間
 　FlickGesture.ScreenFlickVector: スワイプした方向
 　SimpleScaleGesture.LocalDeltaScale: （フレームごとの）拡大・縮小したレート 
      ZoomRate = 。。。前前LocalDeltaScale * 前LocalDeltaScale * LocalDeltaScale
   SimpleRotateGesture.DeltaRotation: （フレームごとの）回転角度
   　　Rotation = 。。。前前DeltaRotation * 前DeltaRotaion * DelataRotation
