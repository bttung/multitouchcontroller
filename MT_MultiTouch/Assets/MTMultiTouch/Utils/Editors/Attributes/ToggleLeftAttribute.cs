﻿using UnityEngine;

namespace MTMultiTouch.Utils.Editors.Attributes
{
    /// <summary>
    /// <para>An attribute to use with ToggleLeft item drawer.</para>
    /// <para><b>For internal use only!</b></para>
    /// </summary>
    public class ToggleLeftAttribute : PropertyAttribute
    {}
}