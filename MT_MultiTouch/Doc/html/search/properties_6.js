var searchData=
[
  ['id',['Id',['../interface_touch_script_1_1_i_touch.html#af2f24a488bf30f9b9eb9538c10f288d3',1,'TouchScript::ITouch']]],
  ['ignorechildren',['IgnoreChildren',['../class_touch_script_1_1_gestures_1_1_press_gesture.html#a2cff124a37ad6452c3193a2f0390ec1b',1,'TouchScript.Gestures.PressGesture.IgnoreChildren()'],['../class_touch_script_1_1_gestures_1_1_release_gesture.html#a9b70b5ad65310d144baaeefef580399a',1,'TouchScript.Gestures.ReleaseGesture.IgnoreChildren()']]],
  ['instance',['Instance',['../class_touch_script_1_1_hit_1_1_touch_hit_factory.html#a2a3964c96bc92cb6ac301b6045e00986',1,'TouchScript.Hit.TouchHitFactory.Instance()'],['../class_touch_script_1_1_touch_manager.html#a7b5dbd6ed6d2856292f65fa62c525166',1,'TouchScript.TouchManager.Instance()']]]
];
