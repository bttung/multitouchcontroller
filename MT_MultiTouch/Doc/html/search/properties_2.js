var searchData=
[
  ['delegate',['Delegate',['../class_touch_script_1_1_gestures_1_1_gesture.html#a21efe860928ca5671e749e77a511e8ea',1,'TouchScript::Gestures::Gesture']]],
  ['deltarotation',['DeltaRotation',['../class_touch_script_1_1_gestures_1_1_simple_1_1_simple_rotate_gesture.html#a1d9a776914feb11c140f6c02940ca6fd',1,'TouchScript::Gestures::Simple::SimpleRotateGesture']]],
  ['direction',['Direction',['../class_touch_script_1_1_gestures_1_1_flick_gesture.html#a9cbca014501eaa2844063f5635c83efa',1,'TouchScript::Gestures::FlickGesture']]],
  ['displaydevice',['DisplayDevice',['../interface_touch_script_1_1_i_touch_manager.html#a5dc363804c1d0c622670a8e9ecb2088c',1,'TouchScript.ITouchManager.DisplayDevice()'],['../class_touch_script_1_1_touch_manager.html#aa206d5e84f652a2ec6c3b2bd4b1a5bf1',1,'TouchScript.TouchManager.DisplayDevice()']]],
  ['distancelimit',['DistanceLimit',['../class_touch_script_1_1_gestures_1_1_long_press_gesture.html#abb4f31a7ead22be2a23eb7192d7c4e60',1,'TouchScript.Gestures.LongPressGesture.DistanceLimit()'],['../class_touch_script_1_1_gestures_1_1_tap_gesture.html#a95043d321d28f0d55b36921f0cd238f0',1,'TouchScript.Gestures.TapGesture.DistanceLimit()']]],
  ['dotspercentimeter',['DotsPerCentimeter',['../interface_touch_script_1_1_i_touch_manager.html#a5f11947bd27d0152117f3362293a113e',1,'TouchScript::ITouchManager']]],
  ['dpi',['DPI',['../class_touch_script_1_1_devices_1_1_display_1_1_display_device.html#a5bf26a4a91eea7d62077534e80bec3dd',1,'TouchScript.Devices.Display.DisplayDevice.DPI()'],['../interface_touch_script_1_1_devices_1_1_display_1_1_i_display_device.html#a12971696bc6a161536a6ca5a53d43be4',1,'TouchScript.Devices.Display.IDisplayDevice.DPI()'],['../interface_touch_script_1_1_i_touch_manager.html#a8937084c3726c8bf17fdad95eccec655',1,'TouchScript.ITouchManager.DPI()']]]
];
