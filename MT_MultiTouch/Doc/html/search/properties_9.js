var searchData=
[
  ['name',['Name',['../class_touch_script_1_1_devices_1_1_display_1_1_display_device.html#af7caa8f9743cb19175648084daf521b0',1,'TouchScript.Devices.Display.DisplayDevice.Name()'],['../interface_touch_script_1_1_devices_1_1_display_1_1_i_display_device.html#af531c8b1bf10d3d0a0f83b1c9add0c17',1,'TouchScript.Devices.Display.IDisplayDevice.Name()']]],
  ['normal',['Normal',['../interface_touch_script_1_1_hit_1_1_i_touch_hit3_d.html#ae74c1087675a6818a901badbabec3e56',1,'TouchScript::Hit::ITouchHit3D']]],
  ['normalizedscreenposition',['NormalizedScreenPosition',['../class_touch_script_1_1_gestures_1_1_gesture.html#acd20f4aa1c0c73d94fd629a45188edd3',1,'TouchScript::Gestures::Gesture']]],
  ['numberoftapsrequired',['NumberOfTapsRequired',['../class_touch_script_1_1_gestures_1_1_tap_gesture.html#a0dcb3f551a0ae550ee2253552fd0d710',1,'TouchScript::Gestures::TapGesture']]],
  ['numberoftouches',['NumberOfTouches',['../interface_touch_script_1_1_i_touch_manager.html#aa924702b3f8257efe5a3623c295c0531',1,'TouchScript::ITouchManager']]]
];
